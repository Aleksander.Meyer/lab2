package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Frigde implements IFridge {

   ArrayList<FridgeItem> items = new ArrayList<FridgeItem>();
    

    public int nitemsInFrigde() {
        return items.size();
    }

    
    public int totalSize() {
        return 20;
    }    
    
    public void emptyFrigde() {
        items.clear();
    }
    
    public boolean placeIn(FridgeItem item) {
        if (!items.contains()< this.totalSize()){ 
        items.add (item);
        return true;
    } else { 
        return false; 
        }
    }

    
    public void takeOut(FridgeItem item){
        if(!items.contains(item))
          throw new IllegalArgumentException("item is not in the frigde");
          items.remove( item );
    }
    

    
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> ExpItems= getExpiredItems();
        items.removeAll(ExpItems);
        return ExpItems;
        }
        private List<FridgeItem> getExpiredItems(){  
            List<FridgeItem> ExpItems= new ArrayList<FridgeItem>();
            for(FridgeItem item: items) 
            if(item.hasExpired()){
                ExpItems.add(item);
            }
        }
        return ExpItems
    }    

}

